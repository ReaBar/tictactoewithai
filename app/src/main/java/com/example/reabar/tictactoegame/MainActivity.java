package com.example.reabar.tictactoegame;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MainActivity extends ActionBarActivity {

    private boolean turn = true, whoStarts = false,winner = false; //true = 'X', false = '0'
    private boolean[] cellFree = new boolean[9];
    private boolean ai = true,aiFlag = true,cheat = false,skip = true;
    private char[] characterInCell = new char[9];
    private int playerXWins = 0,player0Wins = 0, ties = 0,level = 2,move,stepsCounter = 0;
    private char circle = '0', x = 'X', nil = 'N';
    private Button btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btnNewGame;
    private List<Integer> availablePoints;
    private List<PointsAndScores> rootsChildrenScores;
    private Button[] buttonsArray;
    private ImageView[] imageViewArray;
    private ImageView imageViewBtn1,imageViewBtn2,imageViewBtn3,imageViewBtn4,imageViewBtn5,imageViewBtn6,imageViewBtn7,imageViewBtn8,imageViewBtn9;
    private TextView playerX,player0,gameTies,turnText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Arrays.fill(cellFree, true);
        Arrays.fill(characterInCell, nil);

        playerX = (TextView) findViewById(R.id.textViewPlayerX);
        player0 = (TextView) findViewById(R.id.textViewPlayer0);
        gameTies = (TextView) findViewById(R.id.textViewTies);
        turnText = (TextView) findViewById(R.id.textViewWhosTurn);

        player0.setText("Player '0': " + player0Wins);
        playerX.setText("Player 'X': " + player0Wins);
        gameTies.setText("Ties: " + ties);

        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);

        buttonsArray = new Button[]{btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9};

        imageViewBtn1 = (ImageView) findViewById(R.id.imageViewInsideBtn1);
        imageViewBtn2 = (ImageView) findViewById(R.id.imageViewInsideBtn2);
        imageViewBtn3 = (ImageView) findViewById(R.id.imageViewInsideBtn3);
        imageViewBtn4 = (ImageView) findViewById(R.id.imageViewInsideBtn4);
        imageViewBtn5 = (ImageView) findViewById(R.id.imageViewInsideBtn5);
        imageViewBtn6 = (ImageView) findViewById(R.id.imageViewInsideBtn6);
        imageViewBtn7 = (ImageView) findViewById(R.id.imageViewInsideBtn7);
        imageViewBtn8 = (ImageView) findViewById(R.id.imageViewInsideBtn8);
        imageViewBtn9 = (ImageView) findViewById(R.id.imageViewInsideBtn9);

        imageViewArray = new ImageView[]{imageViewBtn1,imageViewBtn2,imageViewBtn3,imageViewBtn4,imageViewBtn5,imageViewBtn6,imageViewBtn7,imageViewBtn8,imageViewBtn9};

        final Toast cellTaken = Toast.makeText(getApplicationContext(), "Cell was already selected", Toast.LENGTH_SHORT);
        whosTurn();

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cellFree[0]) {
                    drawCharacter(0);
                    if (ai && !winner) aiPlay();
                } else {
                    cellTaken.show();
                }
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cellFree[1]) {
                    drawCharacter(1);
                    if(ai && !winner) aiPlay();
                } else {
                    cellTaken.show();
                }
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cellFree[2]) {
                    drawCharacter(2);
                    if(ai && !winner) aiPlay();
                } else {
                    cellTaken.show();
                }
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cellFree[3]) {
                    drawCharacter(3);
                    if(ai && !winner) aiPlay();
                } else {
                    cellTaken.show();
                }
            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cellFree[4]) {
                    drawCharacter(4);
                    if(ai && !winner) aiPlay();
                } else {
                    cellTaken.show();
                }
            }
        });

        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cellFree[5]) {
                    drawCharacter(5);
                    if(ai && !winner) aiPlay();
                } else {
                    cellTaken.show();
                }
            }
        });

        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cellFree[6]) {
                    drawCharacter(6);
                    if(ai && !winner) aiPlay();
                } else {
                    cellTaken.show();
                }
            }
        });

        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cellFree[7]) {
                    drawCharacter(7);
                    if(ai && !winner) aiPlay();
                } else {
                    cellTaken.show();
                }
            }
        });

        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cellFree[8]) {
                    drawCharacter(8);
                    if(ai && !winner) aiPlay();
                } else {
                    cellTaken.show();
                }
            }
        });

        btnNewGame = (Button) findViewById(R.id.btnNewGame);

        btnNewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Arrays.fill(cellFree, true);
                Arrays.fill(characterInCell, nil);
                turn = true;
                winner = false;
                cheat = false;
                skip = true;
                stepsCounter=0;
                for(int i = 0; i<buttonsArray.length ; i++){
                    buttonsArray[i].setBackgroundResource(R.drawable.empty);
                    buttonsArray[i].setEnabled(true);
                    imageViewArray[i].setBackgroundResource(R.drawable.empty);
                }
                whosTurn();
                if(whoStarts){
                    turn = !turn;
                    Random rand = new Random();
                    int randomInt = rand.nextInt(9);
                    drawCharacter(randomInt);
                }
            }
        });
    }

    public void drawCharacter(int index){
        Button tempBtn = buttonsArray[index];
        cellFree[index] = false;
        if(!turn){
            tempBtn.setBackgroundResource(R.drawable.o);
            characterInCell[index] = circle;
        }
        else if(turn){
            tempBtn.setBackgroundResource(R.drawable.x);
            characterInCell[index] = x;
        }
        stepsCounter++;
        turn = !turn;
        whosTurn();
        if(stepsCounter>4) checkForWin();
    }

    public void aiPlay(){
        Random rand = new Random();
        if(stepsCounter!=9){
            if(level == 1){
                Log.d("level",String.valueOf(level));
                List<Integer> availableCells = this.getAvailableStates();
                int num = availableCells.get(rand.nextInt(availableCells.size()));
                drawCharacter(num);
            }
            else{
                if(level == 2){
                    final Integer[] levels = new Integer[]{3,3,1,3,3,1,3,1,3,3};
                    level = levels[rand.nextInt(levels.length)];
                    aiPlay();
                    level = 2;
//                    if(cheat){
//                        level = levels[rand.nextInt(levels.length)];
//                        if(!skip){
//                            skip = !skip;
//                            aiPlay();
//                        }
//                        level = 2;
//                        cheat = !cheat;
//                    }
//                    else if(!cheat){
//                        level = levels[rand.nextInt(levels.length)];
//                        if(skip){
//                            skip = !skip;
//                            aiPlay();
//                        }
//                        level = 2;
//                        cheat = !cheat;
//                    }
                }
                else if(level == 3){
                    Log.d("level",String.valueOf(level));
                    callMinimax(0, turn);
                    move = returnBestMove();
                    drawCharacter(move);
                }
            }
        }
    }

    public void whosTurn() {
        if (turn) {
            turnText.setBackgroundResource(R.drawable.xplay);
        } else {
            turnText.setBackgroundResource(R.drawable.oplay);
        }
    }

    public char gameOver(){
        aiFlag = false;
        char row = checkForWinRow();
        char column = checkForWinColumn();
        char diagonal = checkForWinDiagonal();

        if (row != nil || column != nil || diagonal != nil) {
            if (row == circle || column == circle || diagonal == circle) {
                return circle;
            } else if (row == x || column == x || diagonal == x) {
                return x;
            }
        }
        return nil;
    }

    public void checkForWin() {
        aiFlag = true;
        char row = checkForWinRow();
        char column = checkForWinColumn();
        char diagonal = checkForWinDiagonal();

        if (row != nil || column != nil || diagonal != nil)
        {
            if (row == circle || column == circle || diagonal == circle) {
                turnText.setBackgroundResource(R.drawable.owin);
                player0Wins++;
                player0.setText("Player '0': " + player0Wins);
            } else if (row == x || column == x || diagonal == x) {
                turnText.setBackgroundResource(R.drawable.xwin);
                playerXWins++;
                playerX.setText("Player 'X': " + playerXWins);
            }

            for(int i = 0; i<buttonsArray.length; i++){
                buttonsArray[i].setEnabled(false);
            }

            winner = true;
        } else if (stepsCounter == 9) {
            turnText.setBackgroundResource(R.drawable.nowin);
            ties++;
            gameTies.setText("Ties: " + ties);
        }
    }

    public char checkForWinRow() {
        if (characterInCell[0] != nil && characterInCell[0] == characterInCell[1] && characterInCell[1] == characterInCell[2]) {
            if(aiFlag){
                imageViewBtn1.setBackgroundResource(R.drawable.mark8);
                imageViewBtn2.setBackgroundResource(R.drawable.mark8);
                imageViewBtn3.setBackgroundResource(R.drawable.mark8);
            }
            return characterInCell[0];
        } else if (characterInCell[3] != nil && characterInCell[3] == characterInCell[4] && characterInCell[4] == characterInCell[5]) {
            if(aiFlag) {
                imageViewBtn4.setBackgroundResource(R.drawable.mark8);
                imageViewBtn5.setBackgroundResource(R.drawable.mark8);
                imageViewBtn6.setBackgroundResource(R.drawable.mark8);
            }
            return characterInCell[3];
        } else if (characterInCell[6] != nil && characterInCell[6] == characterInCell[7] && characterInCell[7] == characterInCell[8]) {
            if(aiFlag) {
                imageViewBtn7.setBackgroundResource(R.drawable.mark7);
                imageViewBtn8.setBackgroundResource(R.drawable.mark7);
                imageViewBtn9.setBackgroundResource(R.drawable.mark7);
            }
            return characterInCell[6];
        }
        return nil;
    }

    public char checkForWinColumn() {
        if (characterInCell[0] != nil && characterInCell[0] == characterInCell[3] && characterInCell[3] == characterInCell[6] ) {
            if(aiFlag) {
                imageViewBtn1.setBackgroundResource(R.drawable.mark4);
                imageViewBtn4.setBackgroundResource(R.drawable.mark4);
                imageViewBtn7.setBackgroundResource(R.drawable.mark4);
            }
            return characterInCell[0];
        } else if (characterInCell[1] != nil && characterInCell[1] == characterInCell[4] && characterInCell[4] == characterInCell[7]) {
            if(aiFlag) {
                imageViewBtn2.setBackgroundResource(R.drawable.mark4);
                imageViewBtn5.setBackgroundResource(R.drawable.mark4);
                imageViewBtn8.setBackgroundResource(R.drawable.mark4);
            }
            return characterInCell[1];
        } else if (characterInCell[2] != nil && characterInCell[2] == characterInCell[5] && characterInCell[5] == characterInCell[8]) {
            if(aiFlag) {
                imageViewBtn3.setBackgroundResource(R.drawable.mark4);
                imageViewBtn6.setBackgroundResource(R.drawable.mark4);
                imageViewBtn9.setBackgroundResource(R.drawable.mark4);
            }
            return characterInCell[2];
        }
        return nil;
    }

    public char checkForWinDiagonal() {
        if (characterInCell[0] != nil && characterInCell[0] == characterInCell[4] && characterInCell[4] == characterInCell[8]) {
            if(aiFlag) {
                imageViewBtn1.setBackgroundResource(R.drawable.mark1);
                imageViewBtn5.setBackgroundResource(R.drawable.mark1);
                imageViewBtn9.setBackgroundResource(R.drawable.mark1);
            }
            return characterInCell[0];
        } else if (characterInCell[2] != nil && characterInCell[2] == characterInCell[4] && characterInCell[4] == characterInCell[6]) {
            if(aiFlag) {
                imageViewBtn3.setBackgroundResource(R.drawable.mark2);
                imageViewBtn5.setBackgroundResource(R.drawable.mark2);
                imageViewBtn7.setBackgroundResource(R.drawable.mark2);
            }
            return characterInCell[2];
        }
        return nil;
    }

    public List<Integer> getAvailableStates() {
        availablePoints = new ArrayList<Integer>();
        int j = 0;
        for(int i = 0; i < cellFree.length ; i++) {
            if(cellFree[i]) {
                availablePoints.add(j,i);
                j++;
            }
        }
        return availablePoints;
    }

    public void placeAMove(int point, char player) {
        characterInCell[point] = player;   //player = 'X' or '0'
        cellFree[point] = false;
    }

    class PointsAndScores {
        int score;
        int point;

        PointsAndScores(int score, int point) {
            this.score = score;
            this.point = point;
        }
    }

    public void callMinimax(int depth, boolean turn){
        rootsChildrenScores = new ArrayList<PointsAndScores>();
        minimax(depth, turn);
    }

    public int returnBestMove() {
        int MAX = -100000;
        int best = -1;

        for (int i = 0; i < rootsChildrenScores.size(); ++i) {
            if (MAX < rootsChildrenScores.get(i).score) {
                MAX = rootsChildrenScores.get(i).score;
                best = i;
            }
        }

        return rootsChildrenScores.get(best).point;
    }

    public int minimax(int depth,boolean turn){
        char winner = gameOver();
        if(winner == circle) return +1;
        else if (winner == x) return -1;

        List<Integer> pointsAvailable = getAvailableStates();
        if (pointsAvailable.isEmpty()) return 0;

        List<Integer> scores = new ArrayList<Integer>();

        for (int i = 0; i < pointsAvailable.size(); ++i) {
            int point = pointsAvailable.get(i);
            if (!turn) { //X's turn select the highest from below minimax() call
                placeAMove(point, circle);
                int currentScore = minimax(depth + 1, !turn);
                scores.add(currentScore);
                if(depth == 0)
                    rootsChildrenScores.add(new PointsAndScores(currentScore, point));

            } else if (turn) {//O's turn select the lowest from below minimax() call
                placeAMove(point, x);
                scores.add(minimax(depth + 1, !turn));
            }
            characterInCell[point] = nil; //Reset this point
            cellFree[point] = true; //free the cell
        }
        return turn == false ? returnMax(scores) : returnMin(scores);
    }

    public int returnMin(List<Integer> list) {
        int min = Integer.MAX_VALUE;
        int index = -1;
        for (int i = 0; i < list.size(); ++i) {
            if (list.get(i) < min) {
                min = list.get(i);
                index = i;
            }
        }
        return list.get(index);
    }

    public int returnMax(List<Integer> list) {
        int max = Integer.MIN_VALUE;
        int index = -1;
        for (int i = 0; i < list.size(); ++i) {
            if (list.get(i) > max) {
                max = list.get(i);
                index = i;
            }
        }
        return list.get(index);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem mi[] = new MenuItem[]{menu.findItem(R.id.menu_noviceLevel),menu.findItem(R.id.menu_moderateLevel),menu.findItem(R.id.menu_proLevel)};
        mi[level-1].setChecked(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        String levelString = " level was activated";

        switch (id){
            case R.id.menu_playAgainstAI:
                ai = true;
                break;
            case R.id.menu_playAgainstAFriend:
                ai = false;
                whoStarts = false;
                btnNewGame.performClick();
                break;
            case R.id.menu_HumanStart:
                whoStarts = false;
                btnNewGame.performClick();
                break;
            case R.id.menu_PcStart:
                whoStarts = true;
                btnNewGame.performClick();
                break;
            case R.id.menu_noviceLevel:
                level = 1;
                Toast.makeText(getApplicationContext(),"Novice" +levelString,Toast.LENGTH_SHORT).show();
                item.setChecked(true);
                btnNewGame.performClick();
                break;
            case R.id.menu_moderateLevel:
                level = 2;
                Toast.makeText(getApplicationContext(),"Moderate" +levelString,Toast.LENGTH_SHORT).show();
                item.setChecked(true);
                btnNewGame.performClick();
                break;
            case R.id.menu_proLevel:
                level = 3;
                Toast.makeText(getApplicationContext(),"Pro" +levelString,Toast.LENGTH_SHORT).show();
                item.setChecked(true);
                btnNewGame.performClick();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
